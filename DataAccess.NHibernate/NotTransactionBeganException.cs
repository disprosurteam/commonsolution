﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommonSolution.DataAccess.NH
{
    public class NotTransactionBeganException : Exception
    {
        private const string PredefinedMsg = "No se inicio una transacción previamente";
        public NotTransactionBeganException()
            : base(PredefinedMsg)
        { }
    }
}