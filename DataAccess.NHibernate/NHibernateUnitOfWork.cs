﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonSolution.Patterns.UnitOfWork;
using NHibernate;

namespace CommonSolution.DataAccess.NH
{
    public abstract class SkeletonNHUnitOfWork<T> : IUnitOfWork where T : class
    {
        protected ISession _session;

        protected SkeletonNHUnitOfWork(ISession Session)
        {
            this._session = Session;
        }

        public void BeginTransaction()
        {
            if ((_session.Transaction != null) && (_session.Transaction.IsActive))
            {
                _session.BeginTransaction();
            }
        }

        public void Commit()
        {
            if ((_session.Transaction != null) && (_session.Transaction.IsActive))
                _session.Transaction.Commit();
            else
                throw new NotTransactionBeganException();
        }

        public void Rollback()
        {
            if ((_session.Transaction != null) && (_session.Transaction.IsActive))
                _session.Transaction.Rollback();
            else
                throw new NotTransactionBeganException();
        }

        public void Dispose()
        {
            using (var session = _session)
            {
                if ((_session.Transaction != null) && (_session.Transaction.IsActive))
                {
                    _session.Transaction.Rollback();
                }
            }
            _session = null;
        }
    }
}