﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonSolution.Patterns.UnitOfWork
{

    /***
     * http://martinfowler.com/eaaCatalog/unitOfWork.html
     */
    public interface IUnitOfWork : IDisposable
    {
        void BeginTransaction();

        void Commit();

        void Rollback();
    }
}
