﻿using System.Collections.Generic;
using CommonSolution.DataAccess.NH.Model;
using System.Collections.Immutable;

namespace CommonSolution.DataAccess.NHTest
{
    public enum TipoTelefono
    { 
        casa,
        trabajo
    }

    public class Telefono : EntityBase
    {
        protected string _Numero;

        protected TipoTelefono _Tipo = TipoTelefono.casa;

        public virtual string Numero
        {
            get { return this._Numero; }
            set { this._Numero = value; }
        }

        public virtual TipoTelefono Tipo
        {
            get { return this._Tipo; }
            set { this._Tipo = value; }
        }

        public Telefono(TipoTelefono Tipo, string Numero)
        {
            this._Numero = Numero;
            this._Tipo = Tipo;
        }

        public Telefono() { }
    }

    public class Email : EntityBase
    {
        protected string _Direccion;

        public virtual string Direccion 
        {
            get { return this._Direccion; }
            set { this._Direccion = value; }
        }

        public Email (string Direccion)
        {
            this._Direccion = Direccion;
        }

        public Email() {}
    }

    public class Usuario : EntityBase
    {
        protected string _Nombre;
        protected Telefono _TelefonoPrincipal;

        public virtual string Nombre
        {
            get { return this._Nombre; }
            set { this._Nombre = value; }
        }

        public virtual Telefono TelefonoPrincipal
        {
            get { return this._TelefonoPrincipal; }
            set { this._TelefonoPrincipal = value; }
        }

        protected ICollection<Email> _Emails = new HashSet<Email>();

        public virtual ICollection<Email> Emails 
        {
            get { return this._Emails;}
            protected set { this._Emails = value; }
        }

        public Usuario() 
        { 
        }

        public Usuario(string Nombre, Telefono TelefonoPrincipal, int Id)
        {
            this.Nombre = Nombre;
            this.TelefonoPrincipal = TelefonoPrincipal;
            this.Id = Id;
        }

        public Usuario(string Nombre)
        {
            this.Nombre = Nombre;
            this.Emails = new List<Email>();
        }
    }
}
