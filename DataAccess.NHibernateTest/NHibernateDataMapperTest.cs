﻿using System;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.Collections.Generic;
using CommonSolution.Patterns;
using CommonSolution.DataAccess.NH;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using NHibernate.Context;
using NHibernate.Mapping.ByCode;
using System.IO;
using System.Diagnostics;
using System.Text;

namespace CommonSolution.DataAccess.NHTest
{

    public static class ListMethods
    {

        public static Usuario ActualizarNombreCon(this Usuario usuario, string NuevoNombre)
        {
            usuario.Nombre = NuevoNombre;
            return usuario;
        }

        public static Usuario Primero(this IList<Usuario> list)
        {
            return list[0];
        }

        public static Usuario Segundo(this IList<Usuario> list)
        {
            return list[1];
        }
    }

    /*<summary>
     * <see> Teoria
     * http://www.cs.uns.edu.ar/~mfalappa/dae/downloads/Clases/Clase-11.pdf
     * </see>
     * </summary>
     */

    [TestClass]
    public class NHibernateDataMapperTest
    {
        #region Inicialización y Finalizacion de los tests
        private NHibernateDataMapper<Usuario> usuarioDataMapper;
        private static ISessionFactory sessionFactory;
        private LinkedList<ISession> sessionList = new LinkedList<ISession>();
        private static Configuration configuration;

        [ClassInitialize]
        public static void FixtureSetup(TestContext context)
        {
            /*IConvention[] conventions = {
                         PrimaryKey.Name.Is(x => "ID"),
                         DefaultLazy.Never(), 
                         ForeignKey.EndsWith("ID")};
             * */

            configuration = new Configuration();
            configuration.DataBaseIntegration(x =>
            {
                x.Dialect<NHibernate.Dialect.SQLiteDialect>();
                x.Driver<NHibernate.Driver.SQLite20Driver>();
                x.ConnectionString = "FullUri=file:memorydb.db?mode=memory&cache=shared";
                //x.ConnectionString = "Data Source=:memory:;Version=3;New=True;";
                x.LogSqlInConsole = true;
                x.LogFormattedSql = true;
                x.AutoCommentSql = true;
                x.ConnectionProvider<NHibernate.Connection.DriverConnectionProvider>();
                x.ConnectionReleaseMode = ConnectionReleaseMode.OnClose;
            });

            ModelMapper mapper = new ModelMapper();
            mapper.AddMappings(new Type[] { typeof(EmailMap), typeof(TelefonoMap), typeof(UsuarioMap) });

            configuration.AddDeserializedMapping(mapper.CompileMappingForAllExplicitlyAddedEntities(), null);

            // http://stackoverflow.com/questions/4884043/how-to-write-to-console-out-during-execution-of-an-mstest-test
            // http://msdn.microsoft.com/en-us/library/sk36c28t.aspx
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            sessionFactory = configuration.BuildSessionFactory();
        }

        private ISession CreateSession()
        {
            ISession openSession = sessionFactory.OpenSession();
            sessionList.AddLast(openSession);
            return openSession;
        }

        [TestInitialize]
        public void SetUp()
        {
            ISession openSession = this.CreateSession();
            var connection = openSession.Connection;
            new SchemaExport(configuration).Execute(/* Output to console */ true,
                                                    /* Execute script against database */ true,
                                                    /* Drop database */false,
                                                    /* Connection to execute the sql script */ connection,
                                                    /* writer to file */ null);
            this.usuarioDataMapper = new NHibernateDataMapper<Usuario>(openSession);
        }

        [TestCleanup]
        public void TearDown()
        {
            foreach(var session in sessionList)
            {
                session.Close();
            }
            sessionList.Clear();
            Trace.Flush();
        }
        #endregion

        #region Metodos de ayuda
        /**
         * <summary>
         * Configura la ultima ISession creada para confimar los cambios
         * luego de cualquier operación de insertado/modificación/borrado
         * (muy ineficiente)
         * </summary>
         */
        private void dadoConfirmarDespuesDeInsertar()
        {
            this.sessionList.Last.Value.FlushMode = FlushMode.Always;
        }

        private IList<Usuario> cuandoSeTraenTodosLosUsuarios()
        {
            this.sessionList.Last.Value.Flush();
            var usuarios = usuarioDataMapper.findAll();
            return usuarios;
        }

        private IList<Usuario> cuandoSeTraenLosUsuarioPersistidosDesdeUnaNuevaSesion()
        {
            var nuevoUsuarioDataMapper = new NHibernateDataMapper<Usuario>(CreateSession());
            return nuevoUsuarioDataMapper.findAll();
        }

        private Usuario dadoUnNuevoUsuarioPersistido(Telefono Telefono = null)
        {
            dadoConfirmarDespuesDeInsertar();
            Usuario pepe = new Usuario("Pepe");
            if (Telefono != null)
                pepe.TelefonoPrincipal = Telefono;
            usuarioDataMapper.insert(pepe);
            return pepe;
        }
        private Usuario dadoUnNuevoUsuarioPersistido(Email Email, Email Email2)
        {
            dadoConfirmarDespuesDeInsertar();
            Usuario pepe = new Usuario("Pepe");
            if (Email != null) pepe.Emails.Add(Email);
            if (Email2 != null) pepe.Emails.Add(Email2);
            usuarioDataMapper.insert(pepe);
            return pepe;
        }

        private Usuario dadoUnNuevoUsuarioPersistido(Usuario UsuarioTransient)
        {
            dadoConfirmarDespuesDeInsertar();
            usuarioDataMapper.insert(UsuarioTransient);
            return UsuarioTransient;
        }

        private static void entoncesUsuarioDeberiaEstarContenido(Usuario pepe, IList<Usuario> usuarios)
        {
            Assert.IsTrue(usuarios.Contains(pepe));
        }

        private static void entoncesUsuariosDeberianSerDiferentes(Usuario usuario1, Usuario usuario2)
        {
            Assert.AreNotEqual(usuario1, usuario2);
        }

        private void dadoDosNuevosUsuariosPersistidos(Usuario usuario1 = null, Usuario usuario2 = null)
        {
            dadoConfirmarDespuesDeInsertar();

            usuarioDataMapper.insert(usuario1 != null ? usuario1  : new Usuario("Pepe") { TelefonoPrincipal = new Telefono(TipoTelefono.casa, "22") });
            usuarioDataMapper.insert(usuario2 != null ? usuario2 : new Usuario("juampi") { TelefonoPrincipal = new Telefono(TipoTelefono.casa, "11") });
        }

        private static void entoncesComparacionEntreLosMismosDaDiferente(Usuario pepe, Usuario juampi, Usuario pepePersistente, Usuario juampiPersistente)
        {
            Assert.AreEqual(pepePersistente, pepe);
            Assert.AreEqual(juampiPersistente, juampi);
        }

        private void cuandoSeTraenLosUsuarioPersistidos(out Usuario pepePersistente, out Usuario juampiPersistente)
        {
            var usuariosPersistentes = usuarioDataMapper.findAll();

            if ("Pepe".Equals(usuariosPersistentes.Primero().Nombre))
            {
                pepePersistente = usuariosPersistentes.Primero();
                juampiPersistente = usuariosPersistentes.Segundo();
            }
            else
            {
                pepePersistente = usuariosPersistentes.Segundo();
                juampiPersistente = usuariosPersistentes.Primero();
            }
        }

        private void entoncesHaySoloDosUsuariosPersistidosDentroDe(IList<Usuario> usuariosPersistentes)
        {
            Assert.AreEqual(2, usuariosPersistentes.Count);
        }

        private void entoncesCamposDeTelefonosSonIdenticos(Telefono TelefonoEsperado, Telefono TelefonoPersistido)
        {
            Assert.AreEqual<TipoTelefono>(TelefonoEsperado.Tipo, TelefonoPersistido.Tipo);
            Assert.AreEqual<string>(TelefonoEsperado.Numero, TelefonoPersistido.Numero);
        }

        private void entoncesSonIguales(Usuario UsuarioPersistente, Usuario Usuario)
        {
            Assert.IsTrue(UsuarioPersistente.Equals(Usuario));
        }

        private void entoncesEmailSonIguales(ICollection<Email> conjuntoEsperado, ICollection<Email> conjuntoOriginal)
        {
            var listaEsperada = new List<Email>(conjuntoEsperado);
            var listaOriginal = new List<Email>(conjuntoOriginal);

            for (var i = 0; i < listaOriginal.Count; i++)
            {
                Assert.IsTrue(listaEsperada[i].Equals(listaOriginal[i]));
            }
        }

        private void entoncesUsuarioFueActualizadoCorrectamente(Usuario UsuarioViejo, Usuario UsuarioModificado)
        {
            Usuario UsuarioPersistente = this.usuarioDataMapper.findById(UsuarioViejo.Id);
            Assert.AreEqual(UsuarioModificado, UsuarioPersistente);
        }

        private void cuandoSeActualizaUsuarioPersistidoEnNuevaSesionDesde(Usuario NuevaVersion)
        {
            var nuevoUsuarioDataMapper = new NHibernateDataMapper<Usuario>(CreateSession());
            nuevoUsuarioDataMapper.update(NuevaVersion);
            this.sessionList.Last.Value.Flush();
        }

        private void cuandoSeActualizaUsuarioPersistidoDesde(Usuario NuevaVersion)
        {
            this.usuarioDataMapper.update(NuevaVersion);
            this.sessionList.Last.Value.Flush();
        }

        private Usuario dadoUnClonProfundoDe(Usuario Usuario)
        {
            return new Usuario(Usuario.Nombre, Usuario.TelefonoPrincipal, Usuario.Id);
        }

        #endregion

        #region Tests utilizando que trabajan sobre una misma ISession

        [TestMethod]
        public void testGuardarEntidadSimpleConExito()
        {
            Usuario pepe = dadoUnNuevoUsuarioPersistido();

            var usuarios = cuandoSeTraenTodosLosUsuarios();

            entoncesUsuarioDeberiaEstarContenido(pepe, usuarios);
        }

        [TestMethod]
        [TestCategory("MuchosAUno")]
        public void testGuardarEntidadConDependenciaConExito()
        {
            Telefono telefonoTransient = new Telefono(TipoTelefono.trabajo, "4511789");
            Usuario pepe = dadoUnNuevoUsuarioPersistido(telefonoTransient);

            var usuarioPersistido = cuandoSeTraenTodosLosUsuarios().Primero();

            entoncesCamposDeTelefonosSonIdenticos(telefonoTransient, usuarioPersistido.TelefonoPrincipal);
        }

        [TestMethod]
        [TestCategory("MuchosAUno")]
        [TestCategory("VariasSesiones")]
        public void testGuardarEntidadConDependenciaTraerDesdeOtraSesionConExito()
        {
            Telefono telefonoTransient = new Telefono(TipoTelefono.trabajo, "4511789");
            Usuario pepe = dadoUnNuevoUsuarioPersistido(telefonoTransient);

            var usuarioPersistido = cuandoSeTraenLosUsuarioPersistidosDesdeUnaNuevaSesion().Primero();

            entoncesCamposDeTelefonosSonIdenticos(telefonoTransient, usuarioPersistido.TelefonoPrincipal);
        }

        [TestMethod]
        public void testGuardarEntidadesDiferentesConExito()
        {
            dadoDosNuevosUsuariosPersistidos();

            var usuariosPersistentes = cuandoSeTraenTodosLosUsuarios();

            entoncesUsuariosDeberianSerDiferentes(usuariosPersistentes.Primero(), usuariosPersistentes.Segundo());
        }

        [TestMethod]
        public void testGuardarDosEntidadesConExito()
        {
            dadoDosNuevosUsuariosPersistidos();

            var usuariosPersistentes = cuandoSeTraenTodosLosUsuarios();

            entoncesHaySoloDosUsuariosPersistidosDentroDe(usuariosPersistentes);
        }

        [TestMethod]
        public void testEqualTransientPersistentSonIgualesExito()
        {
            Usuario pepe = dadoUnNuevoUsuarioPersistido();

            Usuario pepePersistente = cuandoSeTraenTodosLosUsuarios().Primero();

            entoncesSonIguales(pepePersistente, pepe);
        }

        [TestMethod]
        [TestCategory("MuchosAMuchos")]
        public void testRelacionMuchosAMuchosUnidireccional()
        {
            Usuario pepe = new Usuario("Pepe");
            pepe.Emails.Add(new Email("gdelca5@gmail.com"));
            dadoUnNuevoUsuarioPersistido(pepe);

            Usuario pepePersistente = cuandoSeTraenTodosLosUsuarios().Primero();

            entoncesEmailSonIguales(pepe.Emails, pepePersistente.Emails);
        }

        [TestMethod]
        [TestCategory("MuchosAMuchos")]
        public void testRelacionMuchosUnidireccionalConNuevaSesion()
        {
            Usuario pepe = new Usuario("Pepe");
            pepe.Emails.Add(new Email("gdelca5@gmail.com"));
            dadoUnNuevoUsuarioPersistido(pepe);

            Usuario pepePersistente = cuandoSeTraenLosUsuarioPersistidosDesdeUnaNuevaSesion().Primero();

            entoncesEmailSonIguales(pepe.Emails, pepePersistente.Emails);
        }

        [TestMethod]
        [TestCategory("MuchosAMuchos")]
        [TestCategory("VariasSesiones")]
        public void testRelacionMuchosAMuchosUnidireccionalConVariasSesiones()
        {
            Usuario pepe = new Usuario("Pepe");
            pepe.Emails.Add(new Email("gdelca5@gmail.com"));
            dadoUnNuevoUsuarioPersistido(pepe);

            Usuario pepePersistente = cuandoSeTraenLosUsuarioPersistidosDesdeUnaNuevaSesion().Primero();

            entoncesEmailSonIguales(pepe.Emails, pepePersistente.Emails);
        }

        [TestMethod]
        public void testActualizarEntidad()
        {
            Usuario pepe = dadoUnNuevoUsuarioPersistido(new Usuario("Pepe"));
            
            Usuario clonPepe = dadoUnClonProfundoDe(pepe)
                                .ActualizarNombreCon("JoseCarlos");

            cuandoSeActualizaUsuarioPersistidoDesde(clonPepe);

            entoncesUsuarioFueActualizadoCorrectamente(pepe, clonPepe);
        }

        [TestMethod]
        public void testDevolverEntidadesOrdenadas()
        {
            dadoDosNuevosUsuariosPersistidos();
            var usuarios = usuarioDataMapper.findAll(20, 0, x => x.TelefonoPrincipal.Numero);

        }

        [TestMethod]
        [TestCategory("VariasSesiones")]
        public void testActualizarEntidadEnNuevaSesion()
        {
            Usuario pepe = dadoUnNuevoUsuarioPersistido(new Usuario("Pepe"));

            Usuario clonPepe = dadoUnClonProfundoDe(pepe)
                                .ActualizarNombreCon("JoseCarlos");

            cuandoSeActualizaUsuarioPersistidoEnNuevaSesionDesde(clonPepe);

            entoncesUsuarioFueActualizadoCorrectamente(pepe, clonPepe);
        }
        #endregion
    }
}
