﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns
{
    public enum OrderBy { Ascendent, Descendant }
    public class OrderByAttribute : Attribute
    {
        public OrderBy OrderBy { get; set; }

        public OrderByAttribute(OrderBy OrderBy)
        {
            this.OrderBy = OrderBy;
        }
    }
}
