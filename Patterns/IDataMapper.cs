﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CommonSolution.Patterns
{
    /**
     * Biblio: http://martinfowler.com/eaaCatalog/dataMapper.html
     **/
    public interface IDataMapper<T>
    {
        T insert(T entity);
        T insertOrUpdate(T entity);
        void update(T entity);
        void delete(T entity);
    }
}
