﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Mapping.ByCode.Conformist;
using NHibernate.Mapping.ByCode;

namespace CommonSolution.DataAccess.NHTest
{
    public class TelefonoMap : ClassMapping<Telefono>
    {
        public TelefonoMap()
        {
            Id(x => x.Id, x => x.Generator(Generators.Native));
            Property(x => x.Numero);
            Property(x => x.Tipo, conf => conf.Type<NHibernate.Type.Int32Type>());
        }
    }

    public class UsuarioMap : ClassMapping<Usuario>
    {
        public UsuarioMap()
        {
            Id(x => x.Id, x => x.Generator(Generators.Native));
            Property(x => x.Nombre);
            
            ManyToOne<Telefono>(x => x.TelefonoPrincipal, conf => 
            {
                conf.Cascade(Cascade.All);
            });

             Bag<Email>(x => x.Emails, conf => {
                conf.Key(k =>
                {
                    k.Column("UsuarioId");
                    k.NotNullable(true);
                });
                conf.Cascade(Cascade.All);
                conf.Table("UsuariosEmail");
                //conf.Inverse(true);
            }, 
            r => r.ManyToMany(many => many.Column("EmailId")));
        }
    }

    public class EmailMap : ClassMapping<Email>
    {
        public EmailMap()
        {
            Id(x => x.Id, x => x.Generator(Generators.Native));
            Property(x => x.Direccion, x =>
            {
                x.Lazy(false);
            });
        }
    }
}
