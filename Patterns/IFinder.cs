﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace CommonSolution.Patterns
{
    public interface IFinder<T, IdType>
    {
        /// <summary>
        /// Devuelve todos los objetos que fueron persistidos del tipo generico
        /// </summary>
        /// <param name="pageSize"> Limitar la cantidad de elementos a buscar (-1, devuelve todos) </param>
        /// <param name="pageNumber"> Devuelve el conjunto de elementos correspondientes al limite </param>
        /// <returns></returns>
        IList<T> findAll(int pageSize = -1, int pageNumber = 0, params Expression<Func<T, object>>[] orderBy);

        T findById(int ID, bool BloqueaRegistro = false);
    }
}