﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonSolution.DataAccess.NH;
using CommonSolution.Patterns;
using NHibernate;
using System.Linq.Expressions;
using NHibernate.Criterion;
using System.Reflection;
using Patterns;

namespace CommonSolution.DataAccess.NH
{
    public class NHibernateDataMapper<T> : IDataMapper<T>, IFinder<T, int> where T : class
    {
        protected ISession _dbSession;
        protected KeyValuePair<string, object>[] _consultas;

        public NHibernateDataMapper(ISession Session, params KeyValuePair<string, object>[] Consultas)
        {
            this._dbSession = Session;
            this._consultas = Consultas;
        }

        public T insert(T entity)
        {
            _dbSession.Save(entity);
            return entity;
        }

        public T insertOrUpdate(T entity)
        {
            _dbSession.SaveOrUpdate(entity);
            return entity;
        }

        public void update(T entity)
        {
            _dbSession.Merge(entity);
        }

        public void delete(T entity)
        {
            _dbSession.Delete(entity);
        }

        public IList<T> findAll(int pageSize = -1, int pageNumber = 0, params Expression<Func<T, object>>[] orderBy)
        {
            ICriteria criteria = _dbSession.CreateCriteria(typeof(T));

            if (_consultas != null)
                for (int i = 0; i < _consultas.Length; i++)
                    if (_consultas[i].Key.Length > 0)
                        criteria.Add(Restrictions.Eq(_consultas[i].Key, _consultas[i].Value));

            if (pageSize != -1)
            {
                criteria.SetFirstResult(pageNumber * pageSize);
                if (pageSize > 0)
                {
                    criteria.SetMaxResults(pageSize);
                }
            }
            for (int i = 0; i < orderBy.Length; i++)
            {
                bool ascendant;
                if (orderBy[i] == null) continue;
                var sorder = orderBy[i].ToString();
                var sorders = sorder.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                if (sorders.Length == 3)
                {
                    criteria.CreateAlias(sorders[1], "alias" + sorders[1])
                    .AddOrder(Order.Asc("alias" + sorders[1] + "." + sorders[2]));

                }
                else
                {
                    var property = GetProperty(orderBy[i], out ascendant);
                    if (orderBy[i] != null) criteria.AddOrder(new Order(property.Name, ascendant));
                }
            }
                
            return criteria.List<T>();
        }

        public T findById(int ID, bool BloqueaRegistro = false)
        {
            if (BloqueaRegistro)
                return _dbSession.Get<T>(ID, LockMode.Upgrade);
            else
                return _dbSession.Get<T>(ID);
        }

        #region Metodos Privados
        private PropertyInfo GetProperty(Expression<Func<T, object>> Property, out bool Ascendent) 
        {
            Ascendent = true;

            if (Property == null) return null;
            MemberExpression member = Property.Body as MemberExpression;
            if (member == null)
            {
                if (Property.Body is UnaryExpression)
                    member = ((UnaryExpression)Property.Body).Operand as MemberExpression;

                if (member == null)
                        throw new ArgumentException(string.Format(
                            "Expression '{0}' refers to a method, not a property.", Property.ToString()));
            }

            PropertyInfo propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.", Property.ToString()));

            OrderByAttribute attribute = propInfo.GetCustomAttribute(typeof(OrderByAttribute)) as OrderByAttribute;
            if (attribute != null)
                Ascendent = (attribute.OrderBy == OrderBy.Ascendent);
            return propInfo;
        }
        #endregion
    }
}